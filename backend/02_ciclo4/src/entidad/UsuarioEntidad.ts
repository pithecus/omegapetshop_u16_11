import PerfilEntidad from "./PerfilEntidad";

class UsuarioEntidad {
  public nombreUsuario: string;
  public correoUsuario: string;
  public claveUsuario: string;
  public fechaUsuario: Date;
  public estadoUsuario: number;
  public codPerfil: PerfilEntidad;

  constructor(nom: string,passw: string,correo: string,fecha: Date,est: number,codP: PerfilEntidad
  ) {
    this.nombreUsuario = nom;
    this.correoUsuario = correo;
    this.claveUsuario = passw;
    this.fechaUsuario = fecha;
    this.estadoUsuario = est;
    this.codPerfil = codP;
  }
}
export default UsuarioEntidad;
