import { Router } from "express";
import usuarioControlador from "../controlador/UsuarioControlador";

class UsuarioRuta {
  public rutaApi: Router;
  constructor() {
    this.rutaApi = Router();
    this.lasRutas();
  }
  //llamar los end point
  public lasRutas() {
    this.rutaApi.get("/listado", usuarioControlador.consultar);
    this.rutaApi.post("/crear", usuarioControlador.crear);
    this.rutaApi.delete("/eliminar/:codigo", usuarioControlador.eliminar);
    this.rutaApi.put("/actualizar/:codigo", usuarioControlador.actualizar);
  }
}
const usuarioRuta = new UsuarioRuta();
export default usuarioRuta.rutaApi;
