import {connect} from "mongoose";

const ConexionDB =()=>{
    //variables de configuración
    const urlConexion=String(process.env.DB);
    
    connect(urlConexion)
  .then(() => {
    console.log("Conexión exitosa", urlConexion);
  })
  .catch((elError) => {
    console.log("No se puede conectar a la BD", elError);
  });
};
    export default ConexionDB;


