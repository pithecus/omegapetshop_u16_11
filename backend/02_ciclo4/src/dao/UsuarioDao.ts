import { Response } from "express";
import UsuarioEsquema from "../esquema/UsuarioEsquema";

class UsuarioDao {
  protected static async obtenerUsuario(res: Response): Promise<any> {
    const informacion = await UsuarioEsquema.find().sort({_id:-1}); //ordenar
    res.status(200).json(informacion);
  }

  protected static async crearUsuario(
    correo:any,
    parametros: any,
    res: Response
  ): Promise<any> {
    const existe = await UsuarioEsquema.findOne(correo).exec();
    if (existe) {
      res.status(400).json({ respuesta: "El correo ya existe" });
    } else {
      const objUsuario = new UsuarioEsquema(parametros);
      objUsuario.save((miError, miObjeto) => {
        if (miError) {
          res.status(400).json({ respuesta: "No se pudo crear el usuario" });
        } else {
          res
            .status(200)
            .json({
              respuesta: "Usuario creado correctamente",
              codigo: miObjeto._id,
            });
        }
      });
    }
  }

  protected static async eliminarUsuario(
    identificador: any,
    res: Response
  ): Promise<any> {
    const existe = await UsuarioEsquema.findById(identificador).exec();
    if (existe) {
      UsuarioEsquema.findByIdAndDelete(
        identificador,
        (miError: any, miObjeto: any) => {
          if (miError) {
            res
              .status(400)
              .json({ respuesta: "El usuario no se pudo eliminar" });
          } else {
            res
              .status(200)
              .json({
                respuesta: "Usuario borrado correctamente",
                eliminado: miObjeto,
              });
          }
        }
      );
    } else {
      res.status(400).json({ respuesta: "El Usuario no existe" });
    }
  }
  protected static async actualizarUsuario(
    identificador: any,
    parametros:any,
    res: Response
  ): Promise<any> {
    const existe = await UsuarioEsquema.findById(identificador).exec();
    if (existe) {
      UsuarioEsquema.findByIdAndUpdate(
        {_id:identificador},
        {$set:parametros},
        (miError: any, miObjeto: any) => {
          if (miError) {
            res
              .status(400)
              .json({ respuesta: "El usuario no se pudo actualizar" });
          } else {
            res
              .status(200)
              .json({
                respuesta: "Usuario actualizado correctamente",
                antiguo: miObjeto,
                nuevo:parametros,
              });
          }
        }
      );
    } else {
      res.status(400).json({ respuesta: "El usuario no existe" });
    }
  }
}
export default UsuarioDao;
