import UsuarioDao from "../dao/UsuarioDao";
import { Response, Request } from "express";

class UsuarioControlador extends UsuarioDao {
  
  public consultar(req: Request, res: Response) {
    UsuarioControlador.obtenerUsuario(res);
  }
  public crear(req: Request, res: Response) {
    const elCorreo={correoUsuario:req.body.correoUsuario};
    UsuarioControlador.crearUsuario(elCorreo,req.body, res);
  }
  public eliminar(req: Request, res: Response) {
    UsuarioControlador.eliminarUsuario(req.params.codigo, res);
  }
  public actualizar(req: Request, res: Response) {
    UsuarioControlador.actualizarUsuario(req.params.codigo, req.body,res);
  }
}
const usuarioControlador = new UsuarioControlador();
export default usuarioControlador;
