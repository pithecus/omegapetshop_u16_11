"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const PerfilEsquema_1 = __importDefault(require("../esquema/PerfilEsquema"));
class PerfilDao {
    static obtenerPerfiles(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const informacion = yield PerfilEsquema_1.default.find();
            res.status(200).json(informacion);
        });
    }
    static crearPerfil(parametros, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const existe = yield PerfilEsquema_1.default.findOne(parametros);
            if (existe) {
                res.status(400).json({ respuesta: "El perfil ya existe" });
            }
            else {
                const objPerfil = new PerfilEsquema_1.default(parametros);
                objPerfil.save((miError, miObjeto) => {
                    if (miError) {
                        res.status(400).json({ respuesta: "No se pudo crear perfil" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            respuesta: "Perfil creado correctamente",
                            codigo: miObjeto._id,
                        });
                    }
                });
            }
        });
    }
    static eliminarPerfil(identificador, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const existe = yield PerfilEsquema_1.default.findById(identificador);
            if (existe) {
                PerfilEsquema_1.default.findByIdAndDelete(identificador, (miError, miObjeto) => {
                    if (miError) {
                        res
                            .status(400)
                            .json({ respuesta: "El perfil no se pudo eliminar" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            respuesta: "Perfil borrado correctamente",
                            eliminado: miObjeto,
                        });
                    }
                });
            }
            else {
                res.status(400).json({ respuesta: "El perfil no existe" });
            }
        });
    }
    static actualizarPerfil(identificador, parametros, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const existe = yield PerfilEsquema_1.default.findById(identificador);
            if (existe) {
                PerfilEsquema_1.default.findByIdAndUpdate({ _id: identificador }, { $set: parametros }, (miError, miObjeto) => {
                    if (miError) {
                        res
                            .status(400)
                            .json({ respuesta: "El perfil no se pudo actualizar" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            respuesta: "Perfil actualizado correctamente",
                            antiguo: miObjeto,
                            nuevo: parametros,
                        });
                    }
                });
            }
            else {
                res.status(400).json({ respuesta: "El perfil no existe" });
            }
        });
    }
}
exports.default = PerfilDao;
