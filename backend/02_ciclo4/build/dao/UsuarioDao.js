"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const UsuarioEsquema_1 = __importDefault(require("../esquema/UsuarioEsquema"));
class UsuarioDao {
    static obtenerUsuario(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const informacion = yield UsuarioEsquema_1.default.find().sort({ _id: -1 }); //ordenar
            res.status(200).json(informacion);
        });
    }
    static crearUsuario(correo, parametros, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const existe = yield UsuarioEsquema_1.default.findOne(correo).exec();
            if (existe) {
                res.status(400).json({ respuesta: "El correo ya existe" });
            }
            else {
                const objUsuario = new UsuarioEsquema_1.default(parametros);
                objUsuario.save((miError, miObjeto) => {
                    if (miError) {
                        res.status(400).json({ respuesta: "No se pudo crear el usuario" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            respuesta: "Usuario creado correctamente",
                            codigo: miObjeto._id,
                        });
                    }
                });
            }
        });
    }
    static eliminarUsuario(identificador, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const existe = yield UsuarioEsquema_1.default.findById(identificador).exec();
            if (existe) {
                UsuarioEsquema_1.default.findByIdAndDelete(identificador, (miError, miObjeto) => {
                    if (miError) {
                        res
                            .status(400)
                            .json({ respuesta: "El usuario no se pudo eliminar" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            respuesta: "Usuario borrado correctamente",
                            eliminado: miObjeto,
                        });
                    }
                });
            }
            else {
                res.status(400).json({ respuesta: "El Usuario no existe" });
            }
        });
    }
    static actualizarUsuario(identificador, parametros, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const existe = yield UsuarioEsquema_1.default.findById(identificador).exec();
            if (existe) {
                UsuarioEsquema_1.default.findByIdAndUpdate({ _id: identificador }, { $set: parametros }, (miError, miObjeto) => {
                    if (miError) {
                        res
                            .status(400)
                            .json({ respuesta: "El usuario no se pudo actualizar" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            respuesta: "Usuario actualizado correctamente",
                            antiguo: miObjeto,
                            nuevo: parametros,
                        });
                    }
                });
            }
            else {
                res.status(400).json({ respuesta: "El usuario no existe" });
            }
        });
    }
}
exports.default = UsuarioDao;
