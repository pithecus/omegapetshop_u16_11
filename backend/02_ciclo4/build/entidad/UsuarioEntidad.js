"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UsuarioEntidad {
    constructor(nom, passw, correo, fecha, est, codP) {
        this.nombreUsuario = nom;
        this.correoUsuario = correo;
        this.claveUsuario = passw;
        this.fechaUsuario = fecha;
        this.estadoUsuario = est;
        this.codPerfil = codP;
    }
}
exports.default = UsuarioEntidad;
