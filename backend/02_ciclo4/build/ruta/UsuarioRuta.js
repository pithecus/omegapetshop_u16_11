"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const UsuarioControlador_1 = __importDefault(require("../controlador/UsuarioControlador"));
class UsuarioRuta {
    constructor() {
        this.rutaApi = (0, express_1.Router)();
        this.lasRutas();
    }
    //llamar los end point
    lasRutas() {
        this.rutaApi.get("/listado", UsuarioControlador_1.default.consultar);
        this.rutaApi.post("/crear", UsuarioControlador_1.default.crear);
        this.rutaApi.delete("/eliminar/:codigo", UsuarioControlador_1.default.eliminar);
        this.rutaApi.put("/actualizar/:codigo", UsuarioControlador_1.default.actualizar);
    }
}
const usuarioRuta = new UsuarioRuta();
exports.default = usuarioRuta.rutaApi;
